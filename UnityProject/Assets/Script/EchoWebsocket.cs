﻿using UnityEngine;
using System.Collections;
using WebSocketSharp;

public class EchoWebsocket : MonoBehaviour {
	public WebSocket ws;
		
	void Start (){
		Debug.Log ("start");
		using (ws = new WebSocket ("ws://echo.websocket.org")) {
			ws.OnOpen += (sender, e) =>{
				Debug.Log ("It's open");
			};
			ws.OnMessage += (sender, e) =>{
				Debug.Log ("Teddy write : " + e.Data);
				//ws.Close ();
			};
			ws.OnError += (sender, e) =>{
				Debug.Log ("There's a bug");
			};
			ws.OnClose += (sender, e) =>{
				Debug.Log ("It's close");
			};
		}
		
		ws.Connect ();
		ws.Send ("coucou");
		ws.Send ("coucou2");
	}

}
