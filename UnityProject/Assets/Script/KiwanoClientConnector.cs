﻿using UnityEngine;
using System;
using WebSocketSharp;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Collections;

public class KiwanoClientConnector : MonoBehaviour {

	public static WebSocket ws;
	public string jsonMethod;
	public string jsonNbor;
	public string jsonUpdate;
	public string jsonRemove;
	public static float lat;
	public static float lng;
	public Hashtable nborHashtable = new Hashtable();
	public GameObject playerGO;
	public string urlid;
	public string iid;

	public class Earthvr{
		public float rotationHeadX;
		public float rotationHeadY;
		public float rotationHeadZ;
		public float rotationHeadW;
	}

	public class Appdata {
		public Earthvr earthvr;
	}

	public class Nbor {
		public float angle;
		public string iid;
		public float lat;
		public string urlid;
		public float lng;
		public Appdata appdata;
	}
	
	public GameObject avatarCube;

	public class RemoveReceiver {
		public List<List<string>> removes;
	}
	
	public class NborReceiver {
		public List<Nbor> nbors;
		public string method;
	}
	public List<Nbor> nborsToInstantiate = new List<Nbor>();
	public List<Nbor> nborsToUpdate = new List<Nbor>();
	public List<List<string>> nborsToRemove = new List<List<string>>();
	public NborReceiver nborReceiver;
	public NborReceiver updateReceiver;
	public RemoveReceiver removeReceiver;

	public class MethodReceiver {
		public string method;
	}
	public MethodReceiver methodReceiver;
	public static Nbor mySelf = new Nbor();
	public static Nbor oldMySelf = new Nbor();
	public NborReceiver methodToSend = new NborReceiver();
	public string jsonToSend;
	public static bool toSendTime;
	public GameObject avatarDemo;
	public Transform followObject;


	void Awake () {
		methodToSend.nbors = new List<Nbor>();
		mySelf.appdata = new Appdata ();
		oldMySelf.appdata = new Appdata ();
		mySelf.appdata.earthvr = new Earthvr ();
		oldMySelf.appdata.earthvr = new Earthvr ();
		lat = 48.853131071849205f+ (UnityEngine.Random.Range(-10,10))*Mathf.Pow(10,-5);
		lng = 2.3440458901200145f+ (UnityEngine.Random.Range(-10,10))*Mathf.Pow(10,-5);
		string serverUrl = "ws://zero.hybridearth.net:443";
		urlid= Uri.EscapeDataString("http://hybridearth.net/u/cubeDemo").Replace("%20", "+");
		iid = "evr"+UnityEngine.Random.Range(0,56665)+"demo"+UnityEngine.Random.Range(0,56665);
		string wsuri = serverUrl+ "/?";
		wsuri += "urlid=" + Uri.EscapeUriString(urlid);
		wsuri += "&iid=" + iid.ToString();
		wsuri += "&lat=" + lat.ToString();
		wsuri += "&lng=" + lng.ToString();
		Debug.Log (wsuri);
		ws = new WebSocket (wsuri);
		ws.SetProxy ("http://proxy.rd.francetelecom.fr:8080","","");
		nborHashtable.Add (JsonConvert.SerializeObject (urlid + iid), playerGO);
		using (ws) {
			ws.OnOpen += (sender, e) =>{
				Debug.Log ("It's open");
			};
			ws.OnError += (sender, e) =>{
				Debug.Log ("There's a bug");
			};
			ws.OnClose += (sender, e) =>{
				Debug.Log ("It's close");
			};
			ws.OnMessage += (sender, e) =>{
				//Debug.Log ("Teddy write : " + e.Data);
				jsonMethod =e.Data;
				methodReceiver = JsonConvert.DeserializeObject<MethodReceiver>(jsonMethod);
				if(methodReceiver.method == "neighbors"){
					Neighbors();
				}
				if(methodReceiver.method == "updates"){
					Updates();
				}
				if(methodReceiver.method == "removes"){
					Removes();
				}
			};
		}
		ws.Connect ();
	}
	void Start (){
		followObject.rotation = new Quaternion (180, 0, 0, 0);
	}

	void Update (){
		/*if(toSendTime){
			SendRotation();
		}
		if (mySelf.appdata.earthvr.rotationHeadX != oldMySelf.appdata.earthvr.rotationHeadX) {
			methodToSend.method = "update";
			methodToSend.nbors.Add(mySelf);
			jsonToSend = JsonConvert.SerializeObject(methodToSend);
			ws.Send(jsonToSend);
			methodToSend.nbors.Clear ();
			oldMySelf = mySelf;
		}*/
		/*foreach (Nbor nbor in nborsToInstantiate) {
			if(nbor.iid == "cubeDemo"){
				if(!nborHashtable.ContainsKey(JsonConvert.SerializeObject (nbor.urlid + nbor.iid))){
					GameObject xGo = (GameObject)Instantiate (avatarCube, new Vector3 ((nbor.lat - lat) * Mathf.Pow (10, 5), 0, (nbor.lng - lng) * Mathf.Pow (10, 5)), Quaternion.Euler (0, nbor.angle, 0));
					nborHashtable.Add(JsonConvert.SerializeObject (nbor.urlid + nbor.iid),xGo);
				}
				else{
					if(JsonConvert.SerializeObject (nbor.urlid + nbor.iid) != JsonConvert.SerializeObject (urlid + iid)){
						GameObject xGo = (GameObject)nborHashtable[JsonConvert.SerializeObject (nbor.urlid + nbor.iid)];
						xGo.transform.position = new Vector3 ((nbor.lat - lat) * Mathf.Pow (10, 5), 0, (nbor.lng - lng) * Mathf.Pow (10, 5));
					}
				}
			}
		}
		nborsToInstantiate.Clear ();*/
		foreach (Nbor nbor in nborsToUpdate) {
			if(nbor.urlid == Uri.EscapeDataString("http://hybridearth.net/u/avatarDemo").Replace("%20", "+")){
				/*nbor.appdata = new Appdata ();
				nbor.appdata.earthvr = new Earthvr();*/
				if(!nborHashtable.ContainsKey(JsonConvert.SerializeObject (nbor.urlid + nbor.iid))){
					//GameObject xGo = (GameObject)Instantiate (avatarCube, new Vector3 ((nbor.lat - lat) * Mathf.Pow (10, 5), 0, (nbor.lng - lng) * Mathf.Pow (10, 5)), Quaternion.Euler (0, nbor.angle, 0));
					GameObject xGo = avatarDemo;
					nborHashtable.Add(JsonConvert.SerializeObject (nbor.urlid + nbor.iid),xGo);
				}
				else{
					if(JsonConvert.SerializeObject (nbor.urlid + nbor.iid) != JsonConvert.SerializeObject (urlid + iid)){
						print("readyTorotate");
						GameObject xGo = (GameObject)nborHashtable[JsonConvert.SerializeObject (nbor.urlid + nbor.iid)];
						followObject.rotation = new Quaternion(nbor.appdata.earthvr.rotationHeadX,nbor.appdata.earthvr.rotationHeadY,nbor.appdata.earthvr.rotationHeadZ,nbor.appdata.earthvr.rotationHeadW);
						print(nbor.appdata.earthvr.rotationHeadX);
						print(followObject.rotation);
					}
				}
			}
		}
		nborsToUpdate.Clear ();
		/*foreach (List<string> remove in nborsToRemove) {
			if (nborHashtable.ContainsKey (JsonConvert.SerializeObject (remove[0] + remove[1]))) {
				GameObject xGo = (GameObject)nborHashtable[JsonConvert.SerializeObject (remove[0] + remove[1])];
				Destroy(xGo);
				nborHashtable.Remove (JsonConvert.SerializeObject (remove[0] + remove[1]));
			}
		}
		nborsToRemove.Clear ();*/
	}

	void Neighbors () {
		jsonNbor = jsonMethod;
		nborReceiver = JsonConvert.DeserializeObject<NborReceiver>(jsonNbor);
		nborsToInstantiate = nborReceiver.nbors;
	}
	void Updates () {
		jsonUpdate = jsonMethod;
		//print (jsonMethod);
		updateReceiver = JsonConvert.DeserializeObject<NborReceiver> (jsonUpdate);
		nborsToUpdate = updateReceiver.nbors;
	}

	void Removes(){
		jsonRemove = jsonMethod;
		removeReceiver = JsonConvert.DeserializeObject<RemoveReceiver> (jsonRemove);
		nborsToRemove = removeReceiver.removes;
	}

	void ReceiveMsg () {

	}

	void SendRotation (){
		methodToSend.method = "update";
		methodToSend.nbors.Add(mySelf);
		jsonToSend = JsonConvert.SerializeObject(methodToSend);
		ws.Send(jsonToSend);
		methodToSend.nbors.Clear ();
		oldMySelf = mySelf;
		toSendTime = false;
	}
	
	void OnApplicationQuit (){
		ws.Close ();
	}
}