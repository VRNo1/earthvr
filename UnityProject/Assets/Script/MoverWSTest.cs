﻿using UnityEngine;
using System.Collections;
using Newtonsoft.Json;
using System.IO;
using WebSocketSharp;

public class MoverWSTest : MonoBehaviour {
	
	public GameObject player;
	public OtherPosition recupOther;
	public static string jsonPlayer;
	
	public class OtherPosition
	{
		public float x;
		public float z;
	}
	
	public OtherPosition otherposition = new OtherPosition();
	
	void Start () {
		jsonPlayer = "{\"x\":0.0,\"z\":0.0}";
	}
	
	void Update () {
		recupOther = JsonConvert.DeserializeObject<OtherPosition>(jsonPlayer);
		transform.position = Vector3.MoveTowards(transform.position,new Vector3 (recupOther.x, this.transform.position.y, recupOther.z),5f*Time.deltaTime);
	}
}