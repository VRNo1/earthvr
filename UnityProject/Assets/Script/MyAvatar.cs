﻿using UnityEngine;
using System;
using Newtonsoft.Json;
using System.IO;
using WebSocketSharp;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using OVR;

public class MyAvatar : MonoBehaviour 
{
		
	public GameObject headAvatar;
	public Transform followObject;
	public float xHeadRotation;
	public float yHeadRotation;
	public float zHeadRotation;
	public float wHeadRotation;
	public float timerRotation;

	void Start () 
	{
		timerRotation = 0.05f;
	}

	void Update () 
	{
		timerRotation -= Time.deltaTime;
		if (timerRotation <= 0f) {
			KiwanoConnector.mySelf.appdata.earthvr.rotationHeadX = followObject.rotation.x;
			KiwanoConnector.mySelf.appdata.earthvr.rotationHeadY = followObject.rotation.y;
			KiwanoConnector.mySelf.appdata.earthvr.rotationHeadZ = followObject.rotation.z;
			KiwanoConnector.mySelf.appdata.earthvr.rotationHeadW = followObject.rotation.w-0.3f;
			timerRotation = 0.05f;
			KiwanoConnector.toSendTime = true;
		}
		HeadMovement ();
	}
		
	void HeadMovement()
	{
	headAvatar.transform.localRotation = new Quaternion (followObject.rotation.z, followObject.rotation.y, -followObject.rotation.w+0.3f, followObject.rotation.x);
	}

}
