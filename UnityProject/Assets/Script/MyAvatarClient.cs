﻿using UnityEngine;
using System;
using Newtonsoft.Json;
using System.IO;
using WebSocketSharp;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using OVR;

public class MyAvatarClient : MonoBehaviour 
{
		
	public GameObject headAvatar;
	public Transform followObject;

	void Update () 
	{
		HeadMovement ();
	}
		
	void HeadMovement()
	{
	headAvatar.transform.localRotation = new Quaternion (followObject.rotation.z, followObject.rotation.y, -followObject.rotation.w, followObject.rotation.x);
	}

}
