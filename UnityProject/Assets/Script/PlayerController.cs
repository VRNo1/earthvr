﻿using UnityEngine;
using System;
using Newtonsoft.Json;
using System.IO;
using WebSocketSharp;
using System.Collections;

public class PlayerController : MonoBehaviour
{
	public WebSocket ws;
	public float speed;
	public string json;
	public float now;
	public float timerToSend;
	public Vector3 oldPosition;
	
	
	
	public class OtherPosition
	{
		public float lat;
		public float lng;
		public string iid;
		public string method;
	}
	
	public OtherPosition otherposition = new OtherPosition();

	void Start()
	{
		otherposition.iid = "kjs64547";
		oldPosition = Vector3.zero;
	}
	
	
	void Update()
	{
		if (Input.GetKey (KeyCode.Z) || Input.GetKey (KeyCode.S) || Input.GetKey (KeyCode.D) || Input.GetKey (KeyCode.Q)) {
			if (Input.GetKey (KeyCode.Z))
					rigidbody.MovePosition (rigidbody.position - Vector3.forward * speed * Time.deltaTime);

			if (Input.GetKey (KeyCode.S))
					rigidbody.MovePosition (rigidbody.position + Vector3.forward * speed * Time.deltaTime);

			if (Input.GetKey (KeyCode.D))
					rigidbody.MovePosition (rigidbody.position - Vector3.right * speed * Time.deltaTime);

			if (Input.GetKey (KeyCode.Q))
					rigidbody.MovePosition (rigidbody.position + Vector3.right * speed * Time.deltaTime);
		}
		if(Mathf.Abs(transform.position.x-oldPosition.x) > 0.3f ||  Mathf.Abs(transform.position.z-oldPosition.z) > 0.3f){
			SendMove();
		}
	}

	void SendMove () {
		otherposition.lat = KiwanoConnector.lat+this.transform.position.x*Mathf.Pow(10,-5);
		otherposition.lng = KiwanoConnector.lng+this.transform.position.z*Mathf.Pow(10,-5);
		if (now >= timerToSend) {
			otherposition.method = "update";
			json = JsonConvert.SerializeObject (otherposition);
			KiwanoConnector.ws.Send(json);
			now = 0;
			oldPosition = this.transform.position;
		}
	}
}


