﻿using UnityEngine;
using System.Collections;
using System;
using Newtonsoft.Json;
using System.IO;

public class TestJson : MonoBehaviour {
	
	public class Product
	{
		public string Name;
		public DateTime Expiry;
		public string[] Sizes;
	}
	public string json;
	
	// Use this for initialization
	void Start ()
	{
		Product product = new Product();
		product.Name = "Apple";
		product.Expiry = new DateTime(2008, 12, 28);
		product.Sizes = new string[] { "Small","Medium","Large" };
		
		json = JsonConvert.SerializeObject(product);
		//{
		//  "Name": "Apple",
		//  "Expiry": "2008-12-28T00:00:00",
		//  "Sizes": [
		//    "Small"
		//  ]
		//}
	}

	void OnGUI (){
		GUI.TextArea (new Rect (Screen.width / 2, Screen.height / 2, 100, 100), json);
	}
	
}